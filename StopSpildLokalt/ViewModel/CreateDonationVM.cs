﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows;

namespace StopSpildLokalt.ViewModel
{
    public class CreateDonationVM : INotifyPropertyChanged
    {

        private ObservableCollection<ProductVM> productVMs;
        public ObservableCollection<ProductVM> ProductVMs
        {
            get { return productVMs; }
            set
            {
                productVMs = value;
                OnPropertyRaised("ProductVMs");
            }

        }

        private DonorVM _SelectedDonor;
        public DonorVM SelectedDonor
        {
            get { return _SelectedDonor; }
            set
            {
                _SelectedDonor = value;
                OnPropertyRaised("SelectedDonor");
            }

        }

        public DonationVM SelectedDonation { get; set; }

        public ProductVM SelectedProduct { get; set; }


        public CreateDonationVM()
        {
            productVMs = new ObservableCollection<ProductVM>();
            SelectedDonation = new DonationVM();     
            SelectedDonation.SignatureDate = DateTime.Now; //Default date
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /*
         * Creates and saves the data of a new Donation instance to the database as an entry to the DONATION table.
         * Subsequently saves all the containing data of ProductVMs to the database as PRODUCT and CATALOGITEM entries.
        */
        public Donation CreateDonationAndAddToRepo(ICollection<ProductVM> productVMs, DonorVM donorVM, DateTime donationDate, string donationInitials)
        {
            Product productObj;
            CatalogItem catalogItemObj;
            Donation donationObj = new Donation(donationDate, donationInitials, donorVM.DonorID, true);

            if (donationObj.DonationID != -1)
            {
                foreach (ProductVM pvm in productVMs)
                {
                    catalogItemObj = new CatalogItem(pvm.Barcode, pvm.ProductName, pvm.OriginCountry, pvm.Weight, true);
                    productObj = new Product(pvm.Barcode, pvm.Amount, pvm.LotNumber, pvm.DateType, pvm.Date, donationObj.DonationID, pvm.DiscardedAmount, true);
                    
                }
            }
            return donationObj;
        }

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

    }
}
