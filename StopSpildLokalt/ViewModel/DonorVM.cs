﻿using StopSpildLokalt.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class DonorVM : INotifyPropertyChanged
    {
        private string _donorName;

        public string DonorName
        {
            get { return _donorName; }
            set
            {
                if (value != _donorName)
                {
                    _donorName = value;
                    OnPropertyChanged("DonorName");
                }
            }
        }

        private int _donorID;

        public int DonorID
        {
            get { return _donorID; }
            set
            {
                if (value != _donorID)
                {
                    _donorID = value;
                    OnPropertyChanged("DonorID");
                }
            }
            
        }

        private string _donorAddress;

        public string DonorAddress
        {
            get { return _donorAddress; }
            set
            {
                if (value != _donorAddress)
                {
                    _donorAddress = value;
                    OnPropertyChanged("DonorAddress");
                }
            }
        }

        private string _donorPhoneNumber;

        public string DonorPhoneNumber
        {
            get { return _donorPhoneNumber; }
            set
            {
                if (value != _donorPhoneNumber)
                {
                    _donorPhoneNumber = value;
                    OnPropertyChanged("DonorPhoneNumber");
                }
            }
        }

        public DonorVM()
        {
        }

        public DonorVM(string donorName, string donorAddress, string donorPhoneNumber, int donorID)
        {
            _donorName = donorName;
            _donorAddress = donorAddress;
            _donorPhoneNumber = donorPhoneNumber;
            _donorID = donorID;
        }

        public DonorVM(string donorName, string donorAddress, string donorPhoneNumber)
        {
            _donorName = donorName;
            _donorAddress = donorAddress;
            _donorPhoneNumber = donorPhoneNumber;
        }
        public DonorVM(Donor donor)
        {
            _donorName = donor.DonorName;
            _donorAddress = donor.DonorAddress;
            _donorPhoneNumber = donor.DonorPhone;
            _donorID = donor.DonorID;
        }

        public override string ToString()
        {
            return DonorName + " - " + DonorAddress;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
