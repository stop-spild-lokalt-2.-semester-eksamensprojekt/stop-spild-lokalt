﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class DonationListVM : INotifyPropertyChanged
    {

        public ObservableCollection<DonorVM> DonorVMs { get; set; }

        private ObservableCollection<DonationVM> _donationVMs;

        public ObservableCollection<DonationVM> DonationVMs
        {
            get { return _donationVMs; }
            set
            {
                if (value != _donationVMs)
                {
                    _donationVMs = value;
                    OnPropertyChanged("DonationVMs");
                }
            }
        }

        private ObservableCollection<DateTime?> _possibleDates;

        public ObservableCollection<DateTime?> PossibleDates
        {
            get { return _possibleDates; }
            set
            {
                if (value != _possibleDates)
                {
                    _possibleDates = value;
                    OnPropertyChanged("PossibleDates");
                }
            }
        }

        public DonationVM SelectedDonation { get; set; }

        public DonorVM SelectedDonor { get; set; }

        public ProductVM SelectedProduct { get; set; }

        public DateTime SelectedDate { get; set; }

        public DonationListVM()
        {
            //ConvertDonationToDonationVM();

            ConvertDonerToDonerVM();

        }

        public void ConvertDonationToDonationVM()
        {
            ObservableCollection<DonationVM> obsCollection = new ObservableCollection<DonationVM>();

            foreach (Donation donation in DonationRepository.GetAll())
            {
                obsCollection.Add(new DonationVM(donation.DonationInitials, donation.DonationDate, donation.DonationID, donation.DonorID));
            }
            DonationVMs = obsCollection;
        }

        public void ConvertDonerToDonerVM()
        {
            ObservableCollection<DonorVM> obsCollection = new ObservableCollection<DonorVM>();

            foreach (Donor donor in DonorRepository.GetAll())
            {
                obsCollection.Add(new DonorVM(donor.DonorName, donor.DonorAddress, donor.DonorPhone, donor.DonorID));
            }

            DonorVMs = obsCollection;
        }

        public void PossibleDatesForSelectedDonor()
        {
            List<Donation> donationList = DonationRepository.GetAll();

            List<DateTime?> dateList = new List<DateTime?>();

            foreach (Donation donation in donationList)
            {
                DateTime date = donation.DonationDate;

                if (donation.DonorID == SelectedDonor.DonorID && (dateList.Find(x => x == date) == null))
                {
                    dateList.Add(date);
                }
            }

            PossibleDates = new ObservableCollection<DateTime?>(dateList);
        }

        public void CreatePrintableDonation(DonationVM donationVM)
        {
            List<ProductVM> productVMs = new List<ProductVM>();
            Donor donor = DonorRepository.GetByID(donationVM.DonorID);
            DonorVM donorVM = new DonorVM(donor.DonorName, donor.DonorAddress, donor.DonorPhone, donor.DonorID);

            foreach(var product in ProductRepository.GetAll())
            {
                if(product.DonationID == donationVM.DonationID)
                {
                    CatalogItem catalogItem = CatalogItemRepository.GetById(product.Barcode);
                    ProductVM productVM = new ProductVM(product.ProductID, catalogItem.Name, product.Barcode, catalogItem.OriginCountry, product.Amount, catalogItem.Weight,
                       product.LotNumber, product.BestBeforeDate, product.ExpirationDate, product.DonationID);
                    productVMs.Add(productVM);
                }
            }

            PrintableDonationVM print = new PrintableDonationVM(donationVM, donorVM, productVMs);
            print.SavePrintableDonation();
            
        }

        public void PossibleDonationsForSelectedDonor()
        {
            List<Donation> donationList = DonationRepository.GetAll();

            ObservableCollection<DonationVM> obsCollection = new ObservableCollection<DonationVM>();

            foreach (Donation donation in donationList)
            {
                if (donation.DonorID == SelectedDonor.DonorID)
                {
                    obsCollection.Add(new DonationVM(donation.DonationInitials, donation.DonationDate, donation.DonationID, donation.DonorID));
                }
            }

            DonationVMs = obsCollection;
        }

        public void PossibleDonationsForSelectedDate()
        {
            List<Donation> donationList = DonationRepository.GetAll();

            ObservableCollection<DonationVM> obsCollection = new ObservableCollection<DonationVM>();

            List<DateTime?> dateList = new List<DateTime?>(PossibleDates);

            foreach (Donation donation in donationList)
            {
                if (donation.DonorID == SelectedDonor.DonorID && donation.DonationDate == SelectedDate)
                {
                    obsCollection.Add(new DonationVM(donation.DonationInitials, donation.DonationDate, donation.DonationID, donation.DonorID));
                }
            }

            DonationVMs = obsCollection;
        }
        public void UpdateDonationDonorIDToRepo(DonorVM newDonorVM)
        {
            Donation oldDonation = new Donation(SelectedDonation.DonationID, SelectedDonation.SignatureDate, SelectedDonation.Initials, SelectedDonation.DonorID);
            Donation newDonation = new Donation(SelectedDonation.DonationID, SelectedDonation.SignatureDate, SelectedDonation.Initials, newDonorVM.DonorID);

            DonationRepository.Update(oldDonation, newDonation);
        }


        public void DeleteSelectedDonationToRepo()
        {
            List<Product> donationProducts = ProductRepository.GetAll();

            donationProducts = donationProducts.Where(x => x.DonationID == SelectedDonation.DonationID).ToList();

            foreach(Product product in donationProducts)
            {
                ProductRepository.Delete(product);
            }

            DonationRepository.Delete(DonationRepository.GetByID(SelectedDonation.DonationID));

            DonationVMs.Remove(SelectedDonation);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
