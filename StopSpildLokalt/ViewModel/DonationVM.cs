﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class DonationVM : INotifyPropertyChanged
    {
        private string _initials;

        public string Initials
        {
            get { return _initials; }
            set
            {
                if (value != _initials)
                {
                    _initials = value;
                    OnPropertyChanged("Initials");
                }
            }
        }

        private DateTime _signatureDate;

        public DateTime SignatureDate
        {
            get { return _signatureDate; }
            set
            {
                if (value != _signatureDate)
                {
                    _signatureDate = value;
                    OnPropertyChanged("SignatureDate");
                }
            }
        }

        private int _donationID;

        public int DonationID
        {
            get { return _donationID; }
            set
            {
                if (value != _donationID)
                {
                    _donationID = value;
                }
            }
        }

        private int _donorID;

        public int DonorID
        {
            get { return _donorID; }
            set
            {
                if (value != _donorID)
                {
                    _donorID = value;
                    OnPropertyChanged("DonorID");
                }
            }
        }

        public DonationVM()
        {

        }

        public DonationVM(string initials, DateTime signatureDate, int donationID, int donorID)
        {
            _initials = initials;
            _signatureDate = signatureDate;
            _donationID = donationID;
            _donorID = donorID;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
