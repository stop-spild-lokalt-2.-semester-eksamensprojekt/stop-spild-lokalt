﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class AddProductVM : INotifyPropertyChanged
    {

        private ObservableCollection<string> productCodes;
        public ObservableCollection<string> ProductCodes 
        {
            get { return productCodes; }
            set 
            {
                productCodes = value;
                OnPropertyRaised("ProductCodes");
            }
        }

        private ObservableCollection<ProductVM> productList;
        public ObservableCollection<ProductVM> ProductList 
        {
            get { return productList; }
            set 
            {
                productList = value;
                OnPropertyRaised("ProductList");
            } 
        }

        private ProductVM selectedProduct;

        public ProductVM SelectedProduct 
        {
            get { return selectedProduct; }
            set 
                {
                selectedProduct = value;
                OnPropertyRaised("SelectedProduct");
            }
        }

        //Constructors
        public AddProductVM(ObservableCollection<ProductVM> productVMs)
        {
            productList = new ObservableCollection<ProductVM>();
            productCodes = new ObservableCollection<string>();
            InitProductList(productVMs);
            InitProductCodes();
        }


        /*
         *Returns an updated ProductVM instance with the values contained in a CatalogItem instance matching the barcode 
         *of the ProductVM given to the method, if such a CatalogItem instance exists. If the CatalogItem does not exist, returns null.
        */
        public ProductVM FindCatalogItem(ProductVM productVM)
        {
            ProductVM foundVM = null;
            CatalogItem found;
            try 
            { 
                found = CatalogItemRepository.GetById(productVM.Barcode); 
            }
            catch(ArgumentException e) //Thrown if the barcode given to CatalogItemRepository doesn't match any entries in CATALOGITEM tabel
            {
                found = null;        
            }

            if(found != null)   //Takes information from the CatalogItem instance, adds it to a ProductVM return var
            {
                foundVM = new ProductVM(found.Name, found.Barcode, found.OriginCountry, productVM.Amount, productVM.Weight, productVM.LotNumber,
                    productVM.Date, productVM.DateType, productVM.DonationID, productVM.DiscardedAmount);
            }
            return foundVM;    
        }


        //Initializes ProductList with the contents of the productVMs collection & adds "empty" objects to ProductList, which represent empty rows in the datagrid.
        public void InitProductList(ObservableCollection<ProductVM> productVMs)
        {
            foreach (ProductVM pVM in productVMs)
            {
                ProductList.Add(pVM);
            }
            for (int i = 0; i < 50; i++)
            {
                ProductVM tempProduct = new ProductVM();
                ProductList.Add(tempProduct);
            }
        }

        public void InitProductCodes()
        {
            ObservableCollection<string> newCodes = new ObservableCollection<string>();
            foreach(CatalogItem item in CatalogItemRepository.GetAll())
            {
                if(item.Barcode.Length <= 4)
                {
                    newCodes.Add($"{item.Name}:  {item.Barcode}");
                }
            }
            ProductCodes = newCodes;
        }

        public void CleanProductList()
        {
            ObservableCollection<ProductVM> temp = new ObservableCollection<ProductVM>();

            foreach(ProductVM productVM in ProductList)
            {
                if (!string.IsNullOrWhiteSpace(productVM.ProductName))
                {
                    temp.Add(productVM);
                }
            }
            ProductList = temp;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
    }
}
