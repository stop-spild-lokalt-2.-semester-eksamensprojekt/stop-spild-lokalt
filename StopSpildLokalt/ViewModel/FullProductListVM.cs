﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class FullProductListVM : INotifyPropertyChanged
    {
        private ObservableCollection<ProductVM> _filteredProductVMs;

        public ObservableCollection<ProductVM> FilteredProductVMs
        {
            get { return _filteredProductVMs; }
            set
            {
                if (value != _filteredProductVMs)
                {
                    _filteredProductVMs = value;
                    OnPropertyChanged("FilteredProductVMs");
                }
            }
        }

        public ObservableCollection<ProductVM> ProductVMs { get; set; }

        public ProductVM SelectedProduct { get; set; }

        public string Search { get; set; }

        public string SearchFilter { get; set; }

        public FullProductListVM()
        {
            ProductVMs = new ObservableCollection<ProductVM>();
        }

        public void FilterListByName()
        {
            List<CatalogItem> filteredCatalogItems = CatalogItemRepository.GetAll().Where(x => x.Name.StartsWith(Search, StringComparison.InvariantCultureIgnoreCase)).ToList();

            List<Product> productList = ProductRepository.GetAll();

            ObservableCollection<ProductVM> obsCollection = new ObservableCollection<ProductVM>();

            foreach (CatalogItem catalogItem in filteredCatalogItems)
            {
                List<Product> catalogProductList = productList.FindAll(x => x.Barcode == catalogItem.Barcode);

                foreach (Product product in catalogProductList)
                {
                    DateTime? date = DonationRepository.GetByID(product.DonationID).DonationDate;

                    obsCollection.Add(new ProductVM(product.ProductID, catalogItem.Name, catalogItem.Barcode, catalogItem.OriginCountry, product.Amount,
                        catalogItem.Weight, product.LotNumber, product.BestBeforeDate, product.ExpirationDate, product.DonationID, date, product.DiscardedAmount));

                }
            }

            FilteredProductVMs = obsCollection;
        }

        public void FilterListByLotNumber()
        {
            List<Product> filteredProducts = ProductRepository.GetAll().Where(x => x.LotNumber.StartsWith(Search, StringComparison.InvariantCultureIgnoreCase)).ToList();


            ObservableCollection<ProductVM> obsCollection = new ObservableCollection<ProductVM>();


            if (filteredProducts.Count > 0)
            {

                foreach (Product product in filteredProducts)
                {
                    CatalogItem catalogItem = CatalogItemRepository.GetById(product.Barcode);

                    DateTime? date = DonationRepository.GetByID(product.DonationID).DonationDate;

                    obsCollection.Add(new ProductVM(product.ProductID, catalogItem.Name, catalogItem.Barcode, catalogItem.OriginCountry, product.Amount,
                            catalogItem.Weight, product.LotNumber, product.BestBeforeDate, product.ExpirationDate, product.DonationID, date, product.DiscardedAmount));

                }
            }

            FilteredProductVMs = new ObservableCollection<ProductVM>( obsCollection.OrderBy(x => x.ProductName));
        }

        public void FilterListByBarcode()
        {
            List<Product> filteredProducts = ProductRepository.GetAll().Where(x => x.Barcode.StartsWith(Search, StringComparison.InvariantCultureIgnoreCase)).ToList();

            ObservableCollection<ProductVM> obsCollection = new ObservableCollection<ProductVM>();

            if(filteredProducts.Count > 0)
            {
                foreach(Product product in filteredProducts)
                {
                    CatalogItem catalogItem = CatalogItemRepository.GetById(product.Barcode);

                    DateTime? date = DonationRepository.GetByID(product.DonationID).DonationDate;

                    obsCollection.Add(new ProductVM(product.ProductID, catalogItem.Name, catalogItem.Barcode, catalogItem.OriginCountry, product.Amount,
                            catalogItem.Weight, product.LotNumber, product.BestBeforeDate, product.ExpirationDate, product.DonationID, date, product.DiscardedAmount));
                }
            }
            FilteredProductVMs = obsCollection;
        }

        public void UpdateProductAndCatalogItem()
        {
            CatalogItem oldCatalogItem = CatalogItemRepository.GetById(SelectedProduct.Barcode);
            CatalogItem newCatalogItem = new CatalogItem(SelectedProduct.Barcode, SelectedProduct.ProductName, SelectedProduct.OriginCountry, SelectedProduct.Weight);
            CatalogItemRepository.Update(oldCatalogItem, newCatalogItem);

            Product oldProduct = ProductRepository.GetByID(SelectedProduct.ProductID);
            Product newProduct = new Product(SelectedProduct.Barcode, SelectedProduct.Amount, SelectedProduct.LotNumber, SelectedProduct.DateType, SelectedProduct.Date, SelectedProduct.DonationID, SelectedProduct.DiscardedAmount);
            ProductRepository.Update(oldProduct, newProduct);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
