﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class ProductVM : INotifyPropertyChanged
    {
        //Constructors
        #region Constructors
        public ProductVM(int productID, string productName, string barcode, string originCountry, int amount,
            int weight, string lotNumber, DateTime? bestBeforeDate, DateTime? expirationDate, int donationID, int discardedAmount)

        {
            _productID = productID;
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _donationID = donationID;
            if (!(bestBeforeDate == null))
            {
                _date = bestBeforeDate;
                _dateType = DateType.BF;
            }
            else if (!(expirationDate == null))
            {
                _date = expirationDate;
                _dateType = DateType.SA;
            }
            else
            {
                _dateType = DateType.Ingen;
                _date = null;
            }
            _discardedAmount = discardedAmount;
        }

        public ProductVM(int productID, string productName, string barcode, string originCountry, int amount,
            int weight, string lotNumber, DateTime? bestBeforeDate, DateTime? expirationDate, int donationID)

        {
            _productID = productID;
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _donationID = donationID;
            if (!(bestBeforeDate == null))
            {
                _date = bestBeforeDate;
                _dateType = DateType.BF;
            }
            else if (!(expirationDate == null))
            {
                _date = expirationDate;
                _dateType = DateType.SA;
            }
            else
            {
                _dateType = DateType.Ingen;
                _date = null;
            }
        }

        public ProductVM(int productID, string productName, string barcode, string originCountry, int amount,
            int weight, string lotNumber, DateTime? date, DateTime? date2, int donationID, DateTime? donationDate, int discardedAmount)
        {
            _productID = productID;
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _donationID = donationID;
            _donationDate = donationDate;
            if (!(date == null))
            {
                _date = date;
                _dateType = DateType.BF;
            }
            else if (!(date2 == null))
            {
                _date = date2;
                _dateType = DateType.SA;
            }
            else
            {
                _dateType = DateType.Ingen;
                _date = null;
            }

            _discardedAmount = discardedAmount;
        }
        public ProductVM(string productName, string barcode, string originCountry, int amount,
            int weight, string lotNumber, DateTime? date, DateType dateType, int donationID, int discardedAmount)
        {
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _donationID = donationID;
            _dateType = dateType;
            _date = date;
            _discardedAmount = discardedAmount;
        }

        public ProductVM(string productName, string barcode, string originCountry, int amount,
        int weight, string lotNumber, DateTime? date, DateType dateType, int discardedAmount)
        {
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _dateType = dateType;
            _date = date;
            _discardedAmount = discardedAmount;
        }

        public ProductVM(int productID, string productName, string barcode, string originCountry, int amount,
            int weight, string lotNumber, DateTime? date, DateType dateType, int donationID, int discardedAmount)
        {
            _productID = productID;
            _productName = productName;
            _barcode = barcode;
            _originCountry = originCountry;
            _amount = amount;
            _weight = weight;
            _lotNumber = lotNumber;
            _donationID = donationID;
            _dateType = dateType;
            _date = date;
            _discardedAmount = discardedAmount;
        }

        public ProductVM()
        {
            this.DateType = DateType.Ingen;
            //this.Date = null;
        }
        #endregion

        //Fields

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            { 
                _productID = value;
                OnPropertyChanged("ProductID");
            }
        }

        private string _productName;

        public string ProductName
        {
            get { return _productName; }
            set 
            { 
                _productName = value;
                OnPropertyChanged("ProductName");
            }
        }
        private string _barcode;

        public string Barcode
        {
            get { return _barcode; }
            set 
            { 
                if(_barcode != value) 
                {
                    UpdateWithCatalogItem(value);
                    _barcode = value;
                }
                OnPropertyChanged("Barcode");
            }
        }
        private string _originCountry;

        public string OriginCountry
        {
            get { return _originCountry; }
            set 
            { 
                _originCountry = value;
                OnPropertyChanged("OriginCountry");
            }
        }
        private int _amount;

        public int Amount
        {
            get { return _amount; }
            set 
            { 
                _amount = value;
                OnPropertyChanged("Amount");
            }
        }
        private int _weight;

        public int Weight
        {
            get { return _weight; }
            set 
            { 
                _weight = value;
                OnPropertyChanged("Weight");
            }
        }
        private string _lotNumber;

        public string LotNumber
        {
            get { return _lotNumber; }
            set 
            { 
                _lotNumber = value;
                OnPropertyChanged("LotNumber");
            }
        }

        private DateTime? _date;

        public DateTime? Date
        {
            get { return _date; }
            set 
            { 
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        private DateType _dateType;

        public DateType DateType
        {
            get { return _dateType; }
            set 
            { 
                _dateType = value;
                OnPropertyChanged("DateType");
            }
        }

        private int _donationID;

        public int DonationID
        {
            get { return _donationID; }
            set 
            { 
                _donationID = value;
                OnPropertyChanged("DonationID");
            }
        }


        private int _discardedAmount;

        public int DiscardedAmount
        {
            get { return _discardedAmount; }
            set
            {
                _discardedAmount = value;
                OnPropertyChanged("DiscardedAmount");
            }
        }

        private DateTime? _donationDate;

        public DateTime? DonationDate
        {
            get { return _donationDate; }
            set 
            { 
                _donationDate = value;
                OnPropertyChanged("DonationDate");
            }
        }

        //Methods

        /*
         *Updates this ProductVM instance with the values contained in a CatalogItem instance matching the barcode 
         *given to the method, if such a CatalogItem instance exists. Returns false if a corresponding CatalogItem does not exist.
        */
        private bool UpdateWithCatalogItem(string barcode)
        {
            bool foundCatalogItem = false;
            CatalogItem catalogItem;
            catalogItem = CatalogItemRepository.GetById(barcode);

            if (catalogItem != null)   //true -> Takes information from the CatalogItem instance, updates ProductVM.
            {
                ProductName = catalogItem.Name;
                OriginCountry = catalogItem.OriginCountry;
                Weight = catalogItem.Weight;
                foundCatalogItem = true;
            }
            return foundCatalogItem;
        }


        public override string ToString()
        {
            return ProductName + " " + Amount + " " + Weight + " " + DonationID;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
