﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class DonorListVM : INotifyPropertyChanged
    {

        private ObservableCollection<DonorVM> donorVMList;

        public ObservableCollection<DonorVM> DonorVMList
        {
            get { return donorVMList; }
            set 
            {
                donorVMList = value;
                OnPropertyChanged("DonorVMList");
            }
        }


        private ObservableCollection<DonorVM> filteredDonors;

        public ObservableCollection<DonorVM> FilteredDonors
        {
            get { return filteredDonors; }
            set
            { 
                filteredDonors = value;
                OnPropertyChanged("FilteredDonor");
            }
        }

        public DonorVM SelectedDonor { get; set; }

        private string donorNameSearch;

        public string DonorNameSearch
        {
            get { return donorNameSearch; }
            set 
            {
                donorNameSearch = value;
                OnPropertyChanged("DonorNameSearch");
            }
        }


        public DonorListVM ()
        {
            DonorVMList = new ObservableCollection<DonorVM>();
            ConvertDonorToDonorVM();
        }

        private void ConvertDonorToDonorVM()
        {
           foreach (Donor donor in DonorRepository.GetAll())
           {
                DonorVMList.Add(new DonorVM(donor.DonorName, donor.DonorAddress, donor.DonorPhone, donor.DonorID));
           }
        }

        private Donor ConvertDonorVMToDonor(DonorVM donorVM)
        {
            Donor donor = new Donor(donorVM.DonorID, donorVM.DonorName, donorVM.DonorAddress, donorVM.DonorPhoneNumber);

            return donor;
        }
        
        public void AddDonorToRepo(/*int donorVMID,*/ string donorVMName, string donorVMAddress, string donorVMPhoneNumber)
        {
            DonorVM donorVM = new DonorVM(donorVMName, donorVMAddress, donorVMPhoneNumber);

            donorVM.DonorID = DonorRepository.Add(ConvertDonorVMToDonor(donorVM));
            DonorVMList.Add(donorVM);
        }

        public void FindDonorByName()
        {
            if(DonorNameSearch != null)
            {
                filteredDonors = new ObservableCollection<DonorVM>();

                List<Donor> filteringDonor = DonorRepository.GetAll().Where(x => x.DonorName.StartsWith(DonorNameSearch, StringComparison.InvariantCultureIgnoreCase)).ToList();

                foreach(Donor donor in filteringDonor)
                {
                    filteredDonors.Add(new DonorVM(donor.DonorName, donor.DonorAddress, donor.DonorPhone));
                }

                DonorVMList = filteredDonors;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
