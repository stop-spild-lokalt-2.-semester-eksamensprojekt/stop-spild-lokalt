﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public enum DateType
    {
        Ingen = 0, //None
        BF = 1, //Bedst før //BestBefore
        SA = 2, //Sidste anvendelse //Expiration
    }
}
