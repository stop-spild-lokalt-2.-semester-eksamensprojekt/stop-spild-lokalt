﻿using ClosedXML.Excel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class PrintableDonationVM
    {
        XLWorkbook printableDonation;
        IXLWorksheet wsDonation;
        DonationVM donationVM;
        DonorVM donorVM;
        DataTable productTable;
        IEnumerable<ProductVM> productVMs;

        public PrintableDonationVM(DonationVM donationVM, DonorVM donorVM, IEnumerable<ProductVM> productVMs)
        {
            printableDonation = new XLWorkbook();
            productTable = new DataTable();
            this.donationVM = donationVM;
            this.donorVM = donorVM;
            this.productVMs = productVMs;
            Initialize();
        }

        public void SavePrintableDonation()
        {
            foreach(var pvm in productVMs)
            {
                productTable.Rows.Add(pvm.Barcode, pvm.ProductName, pvm.OriginCountry, 
                   (pvm.Date.HasValue ? pvm.DateType.ToString() + " " + pvm.Date.Value.ToString("d", new CultureInfo("da-DK")) : "N/A"), 
                    pvm.LotNumber, (pvm.Weight > 0 ? pvm.Weight.ToString() + "g" : "N/A"), pvm.Amount);
            }

            wsDonation.Cell(2, 2).Value = donationVM.SignatureDate.ToString("d", new CultureInfo("da-DK"));
            wsDonation.Cell(3, 2).Value = donorVM.DonorName;
            wsDonation.Cell(4, 2).Value = donorVM.DonorPhoneNumber;
            wsDonation.Cell(5, 2).Value = donorVM.DonorAddress;
            wsDonation.Cell(6, 2).Value = donationVM.Initials;
            wsDonation.Cell(10, 1).InsertTable(productTable.AsEnumerable());
            wsDonation.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            wsDonation.Columns().AdjustToContents();

            var saveFileDialog = new SaveFileDialog() { Filter = "Excel |*.xlsx", Title = "Gem excel fil" };
            saveFileDialog.ShowDialog();
            if (!String.IsNullOrEmpty(saveFileDialog.FileName))
            {
                printableDonation.SaveAs(saveFileDialog.FileName);
            }
            printableDonation.Dispose();
        }

        private void Initialize()
        {
            productTable.Columns.Add("Stregkode", typeof(string));
            productTable.Columns.Add("Varebetegnelse", typeof(string));
            productTable.Columns.Add("Land", typeof(string));
            productTable.Columns.Add("Holdbarhed", typeof(string)); //DateType + Date string equivelants
            productTable.Columns.Add("LOT-nr.", typeof(string));
            productTable.Columns.Add("Vægt", typeof(string));       //Integer value to string + unit of measurement (g)
            productTable.Columns.Add("Antal", typeof(int));

            wsDonation = printableDonation.AddWorksheet($"{donationVM.SignatureDate.ToString("d", new CultureInfo("da-DK"))}  {donorVM.DonorName}"); 
            wsDonation.PageSetup.Margins.Left = 0.3;
            wsDonation.PageSetup.Margins.Right = 0.2;

            wsDonation.Cell(1, 1).Value = "Donationsoplysninger";
            wsDonation.Range("A1:B1").Merge().AddToNamed("Header");

            wsDonation.Cell(2, 1).Value = "Dato";
            wsDonation.Cell(3, 1).Value = "Donor";
            wsDonation.Cell(4, 1).Value = "Donor tlf.";
            wsDonation.Cell(5, 1).Value = "Donor adresse";
            wsDonation.Cell(6, 1).Value = "Registeret af:";
            wsDonation.Range("A2:B6").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wsDonation.Range("A2:B6").Style.Border.BottomBorderColor = XLColor.SteelBlue;
            wsDonation.Range("A9:G9").Merge().AddToNamed("Header");
            wsDonation.Cell(9, 1).Value = "Modtagne varer";

            printableDonation.NamedRange("Header").Ranges.Style.Fill.SetBackgroundColor(XLColor.SteelBlue);
            printableDonation.NamedRange("Header").Ranges.Style.Font.SetFontColor(XLColor.White);
            printableDonation.NamedRange("Header").Ranges.Style.Font.SetBold();
            printableDonation.NamedRange("Header").Ranges.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        }
    }
}
