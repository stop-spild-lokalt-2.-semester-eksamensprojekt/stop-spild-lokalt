﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class UpdateDonationDonorVM
    {
        public List<DonorVM> DonorVMs { get; set; }
        public DonorVM SelectedDonorVM { get; set; }

        public UpdateDonationDonorVM()
        {
            DonorVMs = new List<DonorVM>();

            foreach (Donor donor in DonorRepository.GetAll())
            {
                DonorVM donorVM = new DonorVM(donor);

                DonorVMs.Add(donorVM);
            }
        }
    }
}
