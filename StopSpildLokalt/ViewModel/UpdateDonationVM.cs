﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace StopSpildLokalt.ViewModel
{
    public class UpdateDonationVM : INotifyPropertyChanged
    {
        public delegate void UpdateDonationDelegate();

        public UpdateDonationDelegate updateDonationDel;

        public List<ProductVM> productList = new List<ProductVM>();

        public ObservableCollection<ProductVM> productVMs;

        public List<ProductVM> ProductVMs { get; set; } = new List<ProductVM>();
        public List<ProductVM> UpdatedProductVMs { get; set; } = new List<ProductVM>();
        public List<ProductVM> DeletedProductVMs { get; set; } = new List<ProductVM>();
        public DonationVM DonationVM { get; set; }

        public ProductVM ProductVM { get; set; }

        public ProductVM SelectedProductVM { get; set; }

        private DonorVM updatedDonorVM;

        public DonorVM UpdatedDonorVM
        {
            get { return updatedDonorVM; }
            set
            {
                updatedDonorVM = value;
                OnPropertyChanged("UpdatedDonorVM");
            }
        }


        private DonorVM donorVM;

        public DonorVM DonorVM
        {
            get { return donorVM; }
            set 
            { 
                donorVM = value;
                OnPropertyChanged("DonorVM");
            }
        }

        private ObservableCollection<ProductVM> filteredProductVMs;

        public ObservableCollection<ProductVM> FilteredProductVMs
        {
            get { return filteredProductVMs; }
            set 
            {
                filteredProductVMs = value;
                OnPropertyChanged("FilteredProductVMs");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public UpdateDonationVM(DonationVM donationVM)
        {
            this.DonationVM = donationVM;
            ConvertProductsToProductVMs();
        }

        public void ConvertProductsToProductVMs()
        {
            productVMs = new ObservableCollection<ProductVM>();

            foreach (Product product in ProductRepository.GetAll())
            {
                CatalogItem catalogItem = CatalogItemRepository.GetById(product.Barcode);
                ProductVM productVM = new ProductVM(product.ProductID, catalogItem.Name, product.Barcode, catalogItem.OriginCountry, product.Amount, catalogItem.Weight,
                   product.LotNumber, product.BestBeforeDate, product.ExpirationDate, product.DonationID, product.DiscardedAmount);

                
                productVMs.Add(productVM);
            }
        }

        public ObservableCollection<ProductVM> FilterDonationProducts()
        {
            productList = productVMs.Where(x => x.DonationID == DonationVM.DonationID).ToList<ProductVM>();

            FilteredProductVMs = new ObservableCollection<ProductVM>(productList);

            return filteredProductVMs;
        }

        public void GetDonorVMByID(int donorVMID)
        {
            donorVM = new DonorVM(DonorRepository.GetByID(donorVMID));
        }

        public void AddProductToDonation(ProductVM productVM)
        {
            filteredProductVMs.Add(productVM);
        }

        public void RemoveProductFromDonation()
        {
            filteredProductVMs.Remove(SelectedProductVM);

        }

        public void AddProductsToRepo()
        {
            foreach (ProductVM productVM in ProductVMs)
            {
                Product product = new Product(productVM.Barcode, productVM.Amount, productVM.LotNumber, productVM.Date,
                                            productVM.DateType, productVM.DonationID, productVM.ProductID, productVM.DiscardedAmount);

                CatalogItem catalogItem = new CatalogItem(productVM.Barcode, productVM.ProductName, productVM.OriginCountry, productVM.Weight, true);//Adds to repo in ctor

                ProductRepository.Add(product);
            }
        }
        public void AddUpdatedProductsToRepo()
        {
            foreach (ProductVM productVM in UpdatedProductVMs)
            {
                Product product = new Product(productVM.Barcode, productVM.Amount, productVM.LotNumber, productVM.Date,
                                            productVM.DateType, productVM.DonationID, productVM.ProductID, productVM.DiscardedAmount);
                CatalogItem catalogItem = new CatalogItem(productVM.Barcode, productVM.ProductName, productVM.OriginCountry, productVM.Weight);
                
                CatalogItemRepository.Update(catalogItem, catalogItem);

                ProductRepository.Update(product, product);
            }
        }

        public void DeleteDonationProductsToRepo()
        {
            if (DeletedProductVMs != null)
            {
                foreach (ProductVM productVM in DeletedProductVMs)
                {
                    ProductRepository.Delete(ProductRepository.GetByID(productVM.ProductID));
                }
            }
        }

        private void OnPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
    }
}
