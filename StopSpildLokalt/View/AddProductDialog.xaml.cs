﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for AddProductDialog.xaml
    /// </summary>
    public partial class AddProductDialog : Window
    {
        public AddProductVM AddProductVM { get; }
        public AddProductDialog(ObservableCollection<ProductVM> productViewModels)
        {
            AddProductVM = new AddProductVM(productViewModels);
            DataContext = AddProductVM;
            //DELETE ME? addProductVM.InitProductList(productViewModels);
            InitializeComponent();
        }

        private void Btn_Execute(object sender, RoutedEventArgs e)
        {
            bool foundInvalidSubmission = false;
            int i = 0;
            while(!foundInvalidSubmission && i < AddProductVM.ProductList.Count) 
            {
                if (AddProductVM.ProductList[i].DateType.Equals(DateType.Ingen) && AddProductVM.ProductList[i].Date.HasValue)
                {
                    MessageBox.Show($"Der er udfyldt et 'dato' felt hos {AddProductVM.ProductList[i].ProductName} uden tilsvarende datomærkning", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
                    foundInvalidSubmission = true;
                }
                else if((AddProductVM.ProductList[i].DateType.Equals(DateType.BF) || AddProductVM.ProductList[i].DateType.Equals(DateType.SA))
                    && AddProductVM.ProductList[i].Date == null) 
                {
                    MessageBox.Show($"Der er valgt en datomærkning hos {AddProductVM.ProductList[i].ProductName} som kræver en dato, hvori datofeltet ikke har en gyldig værdi", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
                    foundInvalidSubmission = true;
                }
                else if (AddProductVM.ProductList[i].Amount < 1 && !(string.IsNullOrEmpty(AddProductVM.ProductList[i].ProductName)))
                {
                    MessageBox.Show($"Varen {AddProductVM.ProductList[i].ProductName} har et ugyldigt angivet antal (skal mindt være 1)", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
                    foundInvalidSubmission = true;
                } 
               
                else if (AddProductVM.ProductList[i].DiscardedAmount > AddProductVM.ProductList[i].Amount)
                {
                    MessageBox.Show($"Varen {AddProductVM.ProductList[i].ProductName} har flere kasseret varer end der er varer (må maks være lig antal)", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
                    foundInvalidSubmission = true;
                }
                i++;
            }

            if (!foundInvalidSubmission) 
            {
                AddProductVM.CleanProductList();
                this.DialogResult = true;
            }
        }

        private void Btn_Cancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void dg_CurrentCellChanged(object sender, EventArgs e)
        {
            /*
            var column = dg.CurrentColumn;
            //var cell = dg.CurrentCell;
            var item = dg.CurrentItem;
            int index = dg.Items.IndexOf(dg.CurrentItem);

            if (dg.CurrentColumn == dg.Columns[1] || dg.CurrentColumn == dg.Columns[0])
            {
                dg.ItemsSource = null;
                dg.ItemsSource = AddProductVM.ProductList;
            }

            if (index < dg.Items.Count)
            {
                if (dg.Items[index + 1] != null)
                {
                    dg.ScrollIntoView(dg.Items[index + 1]);
                    dg.SelectedItem = dg.Items[index + 1];
                    dg.CurrentItem = dg.Items[index + 1];
                    dg.BeginEdit();
                }
                
            }

            /*
           

            dg.Focus();
            //dg.CurrentCell = cell;
            dg.CurrentItem = item;
            //dg.CurrentColumn = column;
            if(item != null)
            {
                dg.ScrollIntoView(item);
                dg.BeginEdit();
            }
           */



            //MessageBox.Show("Complete");
            //var currentCell = dg.CurrentCell;
            //codes.Focus();
            //dg.Items.Refresh();
            //dg.Focus();
            //dg.CurrentCell = currentCell;
            //dg.BeginEdit();

            /*
            if (2==2)
            {
                MessageBox.Show("Barcode");
                if(dg.CurrentItem is ProductVM pvm){
                    //pvm.Barcode = "test";
                    //MessageBox.Show(pvm.Barcode);
                    //if(dg.CurrentCell.Column == dg.Columns[0])
                    //var cell = dg.CurrentCell;
                    //var cella = new DataGridCellInfo(dg.Items[1], dg.Columns[3]);
                    //dg.CurrentCell = cella;
                    //dg.CurrentCell = cell;
                    //dg.BeginEdit();
                    //codes.Focus();
                    //dg.Focus();
                    //dg.SelectedItem = pvm;
                    //dg.CurrentCell = cell;
                    //dg.BeginEdit(); 
                    dg.CommitEdit();
                }
            }
            */
        }

        private void dg_KeyDown(object sender, KeyEventArgs e)
        {

            /*
            var uiElement = e.OriginalSource as UIElement;
            if (e.Key == Key.Enter && uiElement != null)
            {
                e.Handled = true;


                int index = dg.Items.IndexOf(dg.CurrentItem);
                if(index < dg.Items.Count)
                {
                    dg.SelectedItem = dg.Items[index + 1];
                }
                
            }
            */

            
            if(e.Key == Key.Return || e.Key == Key.Enter)
            {
                var column = dg.CurrentColumn;
                var cell = dg.CurrentCell;
                var item = dg.CurrentItem;

                if (dg.CurrentColumn == dg.Columns[1] || dg.CurrentColumn == dg.Columns[0])
                {
                    dg.ItemsSource = null;
                    dg.ItemsSource = AddProductVM.ProductList;
                }
                e.Handled = true;


                /*
                codes.Focus();
                dg.Focus();
                dg.CurrentColumn = column;
                dg.CurrentCell = cell;
                if (item != null)
                {
                    dg.ScrollIntoView(item);
                    dg.BeginEdit();
                }
                */
            }
          
        }

        private void dg_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            /*
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                int index = dg.Items.IndexOf(dg.CurrentItem);
                var column = dg.CurrentColumn;
                var cell = dg.CurrentCell;
                var item = dg.CurrentItem;

                if (dg.CurrentColumn == dg.Columns[0])
                {
                    dg.CommitEdit();
                    dg.ItemsSource = null;
                    dg.ItemsSource = AddProductVM.ProductList;

                    if (index < dg.Items.Count)
                    {
                        if (dg.Items[index + 1] != null)
                        {
                            dg.ScrollIntoView(dg.Items[index + 1]);
                            dg.SelectedItem = dg.Items[index + 1];
                            dg.CurrentItem = dg.Items[index + 1];
                            //dg.BeginEdit();
                        }

                    }
                }
                else
                {
                    dg.CommitEdit();
                    if (index < dg.Items.Count-1)
                    {
                        if (dg.Items[index + 1] != null)
                        {
                            dg.ScrollIntoView(dg.Items[index + 1]);
                            dg.SelectedItem = dg.Items[index + 1];
                            dg.CurrentItem = dg.Items[index + 1];
                            dg.CurrentColumn = column;
                            //dg.BeginEdit();
                        }

                    }

                }
                e.Handled = true;
            }
            */
        }
    }
}
