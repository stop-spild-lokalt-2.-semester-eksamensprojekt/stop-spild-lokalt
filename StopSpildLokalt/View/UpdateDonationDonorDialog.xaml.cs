﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for UpdateDonationDonorDialog.xaml
    /// </summary>
    public partial class UpdateDonationDonorDialog : Window
    {
        private UpdateDonationDonorVM updateDonationDonorDialogVM;

        public UpdateDonationDonorDialog()
        {
            InitializeComponent();
            updateDonationDonorDialogVM = new UpdateDonationDonorVM();
            this.DataContext = updateDonationDonorDialogVM;
        }

        private void Btn_Save(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
