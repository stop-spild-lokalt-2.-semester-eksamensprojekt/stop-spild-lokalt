﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for DonorListDialog.xaml
    /// </summary>
    public partial class DonorListDialog : Window
    {
        public DonorListVM donorListVM;

        public DonorListDialog()
        {
            donorListVM = new DonorListVM();
            this.DataContext = donorListVM;
            InitializeComponent();
        }

        private void Btn_Search(object sender, RoutedEventArgs e)
        {
            donorListVM.FindDonorByName();
        }

        private void Btn_CreateDonor(object sender, RoutedEventArgs e)
        {
            CreateDonorDialog createDonorDialog = new CreateDonorDialog();
            DonorVM donorCreateVM = new DonorVM();
            createDonorDialog.Title = "Vælg/opret donor";
            createDonorDialog.DataContext = donorCreateVM;

            if ((bool)createDonorDialog.ShowDialog())
            {
                try
                {
                    donorListVM.AddDonorToRepo(donorCreateVM.DonorName, donorCreateVM.DonorAddress, donorCreateVM.DonorPhoneNumber);
                  
                    MessageBox.Show($"Donoren er blevet oprettet");
                }
                catch (Exception)
                {
                    MessageBox.Show($"Den er gruelig gal!", "FEJL!!?!?!", MessageBoxButton.OK);
                }
            }
        }

        private void Btn_Accept(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

    }
}
