﻿using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for CreateDonationPage.xaml
    /// </summary>
    public partial class CreateDonationPage : Page
    {
        private CreateDonationVM createDonationVM;

        public CreateDonationPage()
        {
            this.createDonationVM = new CreateDonationVM();
            this.DataContext = createDonationVM;

            InitializeComponent();
        }

        private void Btn_AddDonor(object sender, RoutedEventArgs e)
        {
            DonorListDialog donorListDialog = new DonorListDialog();
            donorListDialog.Title = "Vælg/opret donor";

            if ((bool)donorListDialog.ShowDialog())
            {
                try
                {
                    createDonationVM.SelectedDonor = donorListDialog.donorListVM.SelectedDonor;
                }
                catch (Exception)
                {
                    MessageBox.Show($"Den er gruelig gal!", "FEJL!!?!?!", MessageBoxButton.OK);
                }
            }
        }

        private void Btn_AddProducts(object sender, RoutedEventArgs e)
        {
            AddProductDialog addProductDialog = new AddProductDialog(createDonationVM.ProductVMs);
            addProductDialog.ShowDialog();
            if (addProductDialog.DialogResult == true)
            {
                createDonationVM.ProductVMs = addProductDialog.AddProductVM.ProductList;
            }
        }
         
        private void Btn_CreateDonation(object sender, RoutedEventArgs e)
        {
            DonorVM selectedDonor = createDonationVM.SelectedDonor;
            DonationVM selectedDonation = createDonationVM.SelectedDonation;
            bool validDate = DateTime.TryParse(DatePicker_SignatureDate.Text, out DateTime result);

            //Checks if a donor has been selected
            if (selectedDonor == null)
            {
                MessageBox.Show("Donor er ikke valgt", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Checks if ProductsVMs have been added by the user to the collection ProductVMs
            else if(createDonationVM.ProductVMs.Count < 1)
                                                      
            {
                MessageBox.Show("Der skal mindst tilføjes 1 vare til listen af varer, før en donation kan oprettes.", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Message in case the given date via the DatePicker is an invalid DateTime instance.
            else if (!validDate)
            {
                MessageBox.Show("Datoen er ikke udfyldt korrekt", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Checks if the given date lives up to the following constraints: date selected by user has to be no later than today, and no earlier than a year from today.
            else if (!(selectedDonation.SignatureDate <= DateTime.Now && selectedDonation.SignatureDate > DateTime.Now.AddYears(-1))) 
            {
                MessageBox.Show("Ugyldig dato: Den angivne dato kan senest være i dag, eller tidligst angives som værende et år tidligere fra dags dato.",
                    "Ugyldig dato", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Checks if the user has given a valid signature. 
            else if (string.IsNullOrWhiteSpace(selectedDonation.Initials)) 
            {
                MessageBox.Show("Donationen skal underskrives før at den kan oprettes", "OBS", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show("Donor: " + selectedDonor.DonorName + " underskrevet af " + selectedDonation.Initials + " på dato: " + selectedDonation.SignatureDate.ToShortDateString(), "Test", MessageBoxButton.OK);
                createDonationVM.CreateDonationAndAddToRepo(createDonationVM.ProductVMs, createDonationVM.SelectedDonor, createDonationVM.SelectedDonation.SignatureDate, createDonationVM.SelectedDonation.Initials);
            }
        }

    }
}
