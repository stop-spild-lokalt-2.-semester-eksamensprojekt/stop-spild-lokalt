﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for UpdateDonationDialog.xaml
    /// </summary>
    public partial class UpdateDonationDialog : Window
    {
        private DonationVM donationVM;
        public UpdateDonationVM updateDonationVM;

        public DonorVM DonorVM { get; set; }

        int deleteInt;
        int addInt;
        int updateInt;

        public UpdateDonationDialog(DonationVM donationVM)
        {
            InitializeComponent();
            this.donationVM = donationVM;
            updateDonationVM = new UpdateDonationVM(donationVM);
            this.DataContext = this.updateDonationVM;
            updateDonationVM.FilterDonationProducts();
            updateDonationVM.GetDonorVMByID(donationVM.DonorID);

            deleteInt = 0;
            addInt = 0;
            updateInt = 0;
        }

        private void Btn_AddProduct(object sender, RoutedEventArgs e)
        {
            AddSingleProductDialog addSingleProductDialog = new AddSingleProductDialog();

            if (addSingleProductDialog.ShowDialog() == true)
            {
                addSingleProductDialog.ProductVM.DonationID = donationVM.DonationID;
                updateDonationVM.AddProductToDonation(addSingleProductDialog.ProductVM);

                updateDonationVM.ProductVMs.Add(addSingleProductDialog.ProductVM);
                if(addInt < 1)
                {
                    updateDonationVM.updateDonationDel += updateDonationVM.AddProductsToRepo;
                }
                addInt++;
            }
        }

        private void Btn_UpdateDonor(object sender, RoutedEventArgs e)
        {
            UpdateDonationDonorDialog updateDonationDonorDialog = new UpdateDonationDonorDialog();

            if (updateDonationDonorDialog.ShowDialog() == true)
            {
                updateDonationVM.UpdatedDonorVM = (DonorVM)updateDonationDonorDialog.CBO_NewDonor.SelectedItem;//måske overflødig
                updateDonationVM.DonorVM = (DonorVM)updateDonationDonorDialog.CBO_NewDonor.SelectedItem;//måske overflødig

                DonorVM = (DonorVM)updateDonationDonorDialog.CBO_NewDonor.SelectedItem;
                updateDonationVM.DonationVM.DonorID = DonorVM.DonorID;
            }
        }

        private void Btn_DeleteProduct(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Du er ved at slette et produkt, vil du fortsætte?", "Sletning af produkt bekræftelse", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                updateDonationVM.DeletedProductVMs.Add(updateDonationVM.SelectedProductVM);
                updateDonationVM.RemoveProductFromDonation();
                if(deleteInt < 1)
                {
                    updateDonationVM.updateDonationDel += updateDonationVM.DeleteDonationProductsToRepo;
                }
                deleteInt++;
            }
        }

        private void Btn_UpdateProduct(object sender, RoutedEventArgs e)
        {
            UpdateSingleProductDialog updateSingleProductDialog = new UpdateSingleProductDialog(updateDonationVM.SelectedProductVM);

            if(updateSingleProductDialog.ShowDialog() == true)
            {
                updateDonationVM.UpdatedProductVMs.Add(updateSingleProductDialog.productVM);
                if(updateInt < 1)
                {
                    updateDonationVM.updateDonationDel += updateDonationVM.AddUpdatedProductsToRepo;
                }
                updateInt++;
            }
        }

        private void Btn_Save(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
