﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for AddSingleProductDialog.xaml
    /// </summary>
    public partial class AddSingleProductDialog : Window
    {
        public ProductVM ProductVM { get; set; }

        public AddSingleProductDialog()
        {
            ProductVM = new ProductVM();

            this.DataContext = ProductVM;

            InitializeComponent();
        }
        //Makes it so you can only enter chars 1-9 in TBAmount
        private void NumberValidationTBAmount(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NumberValidationTBDiscarded(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Btn_Save(object sender, RoutedEventArgs e)
        {
            if (TB_DiscardedAmount.Text == "")
            {
                TB_DiscardedAmount.Text = "0";
                MessageBox.Show("Det kasseret antal kan ikke være ingen ting");
            }
            else if (TB_Amount.Text == "0" || TB_Amount.Text == "")
            {
                TB_Amount.BorderBrush = Brushes.Red;
                MessageBox.Show("Der mangler antal");
                TB_Amount.Text = "0";
            }
            else if (int.Parse(TB_DiscardedAmount.Text) > int.Parse(TB_Amount.Text))
            {
                TB_DiscardedAmount.BorderBrush = Brushes.Red;
                MessageBox.Show("Det kasserede antal kan ikke være større end varens antal");
            }
            else
            {
                this.DialogResult = true;
            }
        }

        private void TextChangedTB_Discarded(object sender, TextChangedEventArgs e)
        {
            TB_DiscardedAmount.BorderBrush = Brushes.Black;
        }

        private void TextChangedTB_Amount(object sender, TextChangedEventArgs e)
        {
            TB_Amount.BorderBrush = Brushes.Black;

        }
    }
}
