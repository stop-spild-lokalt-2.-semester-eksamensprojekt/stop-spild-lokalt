﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Main.Content = new CreateDonationPage();
            OpretDonation.Foreground = Brushes.SaddleBrown;
        }

        private void Btn_CreateDonation(object sender, RoutedEventArgs e)
        {
            Main.Content = new CreateDonationPage();
            OpretDonation.Foreground = Brushes.SaddleBrown;
            DonationsListe.Foreground = Brushes.Black;
            SamletVareliste.Foreground = Brushes.Black;
        }

        private void Btn_DonationList(object sender, RoutedEventArgs e)
        {
            Main.Content = new DonationListPage();
            OpretDonation.Foreground = Brushes.Black;
            DonationsListe.Foreground = Brushes.SaddleBrown;
            SamletVareliste.Foreground = Brushes.Black;
        }

        private void Btn_FullProductList(object sender, RoutedEventArgs e)
        {
            Main.Content = new FullProductListPage();
            OpretDonation.Foreground = Brushes.Black;
            DonationsListe.Foreground = Brushes.Black;
            SamletVareliste.Foreground = Brushes.SaddleBrown;
        }
    }
}
