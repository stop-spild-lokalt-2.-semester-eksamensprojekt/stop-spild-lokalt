﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for FullProductListPage.xaml
    /// </summary>
    public partial class FullProductListPage : Page
    {
        private FullProductListVM fullProductListVM;
        public FullProductListPage()
        {
            fullProductListVM = new FullProductListVM();
            this.DataContext = fullProductListVM;
            InitializeComponent();
        }

        private void Btn_EditDonation(object sender, RoutedEventArgs e)
        {
            UpdateSingleProductDialog updateSingleProductDialog = new UpdateSingleProductDialog(fullProductListVM.SelectedProduct);
            updateSingleProductDialog.Title = "Rediger donation";

            if (updateSingleProductDialog.ShowDialog() == true)
            {
                fullProductListVM.UpdateProductAndCatalogItem();
            }
        }

        private void Btn_Search(object sender, RoutedEventArgs e)
        {
            if(fullProductListVM.SearchFilter == null)
            {
                MessageBox.Show("Vælg venligst noget at søge på");
            }
            else
            {
                switch(fullProductListVM.SearchFilter)
                {
                    case "System.Windows.Controls.ComboBoxItem: Navn":

                        fullProductListVM.FilterListByName();

                        break;

                    case "System.Windows.Controls.ComboBoxItem: Lot nummer":

                        fullProductListVM.FilterListByLotNumber();

                        break;
                    case "System.Windows.Controls.ComboBoxItem: Stregkode":

                        fullProductListVM.FilterListByBarcode();

                        break;
                }
            }

        }
 
    }
}
