﻿using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StopSpildLokalt.View
{
    /// <summary>
    /// Interaction logic for DonationListPage.xaml
    /// </summary>
    public partial class DonationListPage : Page
    {
        private DonationListVM donationListVM;

        public DonationListPage()
        {
            donationListVM = new DonationListVM();
            this.DataContext = donationListVM;
            InitializeComponent();
        }
        private void CboDonor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            donationListVM.PossibleDonationsForSelectedDonor();
            donationListVM.PossibleDatesForSelectedDonor();
        }

        private void CboDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            donationListVM.PossibleDonationsForSelectedDate();
        }

        private void Btn_UpdateDonation(object sender, RoutedEventArgs e)
        {
            UpdateDonationDialog updateDonationDialog = new UpdateDonationDialog(donationListVM.SelectedDonation);

            if(updateDonationDialog.ShowDialog() == true)
            {
                if (updateDonationDialog.DonorVM != null)
                {
                    donationListVM.UpdateDonationDonorIDToRepo(updateDonationDialog.DonorVM);
                }

                if (updateDonationDialog.updateDonationVM.updateDonationDel != null)
                {
                    updateDonationDialog.updateDonationVM.updateDonationDel();
                }
                donationListVM.PossibleDonationsForSelectedDonor();
            }
        }

        private void Btn_DeleteDonation(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Du er ved at slette en donation, vil du fortsætte?", "Sletning af donation bekræftelse", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                donationListVM.DeleteSelectedDonationToRepo();
            }
        }

        private void Btn_ExportPrintableDonation(object sender, RoutedEventArgs e)
        {
            if(donationListVM.SelectedDonation != null)
            {
                donationListVM.CreatePrintableDonation(donationListVM.SelectedDonation);
            }
            else
            {
                MessageBox.Show("Vælg venligst en donation til udprintning", "OBS", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
    }
}
