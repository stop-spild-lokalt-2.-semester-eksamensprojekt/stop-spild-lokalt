﻿using StopSpildLokalt.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace StopSpildLokalt.Repository
{
    public static class DonorRepository
    {
        private static List<Donor> donors;

        private static string connectionCredentials = "Server=10.56.8.35;Database=A_EKSDB04_2021;User Id=A_EKS04;Password=A_OPENDB04";

        static DonorRepository()
        {
            donors = new List<Donor>();

            if (RepositoryOptions.UseDatabase)
            {
                LoadAllDonor();
            }
            
        }

        public static int Add(Donor donor)
        {
            int addedID = 0;

            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                try
                {
                    SqlDataReader reader;
                    connection.Open();

                    string addDonorQuery = "INSERT INTO DONOR (DonorName, DonorAddress, PhoneNumber) VALUES (@DonorName, @DonorAddress, @PhoneNumber);";

                    SqlCommand addDonorCommand = new SqlCommand(addDonorQuery, connection);
                    addDonorCommand.Parameters.AddWithValue("@DonorName", donor.DonorName);
                    addDonorCommand.Parameters.AddWithValue("@DonorAddress", donor.DonorAddress);
                    addDonorCommand.Parameters.AddWithValue("@PhoneNumber", donor.DonorPhone);

                    int rowsAffected = addDonorCommand.ExecuteNonQuery();

                    if (rowsAffected == 1)
                    {
                        string getDonorIDQuery = "SELECT MAX(DonorId) FROM DONOR";

                        SqlCommand getDonorIDCommand = new SqlCommand(getDonorIDQuery, connection);

                        reader = getDonorIDCommand.ExecuteReader();

                        reader.Read();

                        donor.DonorID = reader.GetInt32(0);

                        donors.Add(donor);

                        addedID = donor.DonorID;
                    }
                }
                catch(Exception e)
                {
                    return addedID;
                    throw e;
                }
                return addedID;
            }
        }

        public static bool Remove(Donor donor)
        {
            bool succes = false;

            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();

                string removeDonorQuery = "DELETE FROM DONOR WHERE DonorId=@ID;";

                SqlCommand removeDonorCommand = new SqlCommand(removeDonorQuery, connection);
                removeDonorCommand.Parameters.AddWithValue("@ID", donor.DonorID);

                int result = removeDonorCommand.ExecuteNonQuery();

                if(result == 1)
                {
                    succes = true;
                }
               
                return succes;
            }
        }

        public static Donor GetByID(int donorID)
        {
            Donor donor = donors.Find(donor => donor.DonorID == donorID);

            if(donor != null)
            {
                return donor;
            }
            else
            {
                return null;
            }

        }

        public static List<Donor> GetAll()
        {
            return donors;
        }

        public static bool UpdateDonor(Donor oldDonor, Donor newDonor)
        {
            bool succes = false;

            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();

                string updateDonorQuery = "UPDATE DONOR SET DonorName=@DonorName, DonorAddress=@DonorAddress, PhoneNumber=@Phonenumber WHERE DonorID=@DonorID;";

                SqlCommand updateDonorCommand = new SqlCommand(updateDonorQuery, connection);
                updateDonorCommand.Parameters.AddWithValue("@DonorID", oldDonor.DonorID);
                updateDonorCommand.Parameters.AddWithValue("@DonorName", newDonor.DonorName);
                updateDonorCommand.Parameters.AddWithValue("@DonorAddress", newDonor.DonorAddress);
                updateDonorCommand.Parameters.AddWithValue("@PhoneNumber", newDonor.DonorPhone);

                int result = updateDonorCommand.ExecuteNonQuery();

                if(result == 1)
                {
                    succes = true;

                    Donor tempDonor;

                    tempDonor = donors.Find(tempDonor => tempDonor.DonorID == oldDonor.DonorID);

                    tempDonor.DonorName = newDonor.DonorName;
                    tempDonor.DonorAddress = newDonor.DonorAddress;
                    tempDonor.DonorPhone = newDonor.DonorPhone;
                }
                return succes;
            }
        }

        public static void LoadAllDonor()
        {
            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();

                string loadAllDonorQuery = "SELECT DonorId, DonorName, DonorAddress, PhoneNumber FROM DONOR";

                SqlCommand loadAllDonorCommand = new SqlCommand(loadAllDonorQuery, connection);

                SqlDataReader dataReader = loadAllDonorCommand.ExecuteReader();

                while(dataReader.Read())
                {
                    donors.Add(new Donor((int)dataReader[0], (string)dataReader[1], (string)dataReader[2], (string)dataReader[3]));
                }
            }
        }
    }
}
