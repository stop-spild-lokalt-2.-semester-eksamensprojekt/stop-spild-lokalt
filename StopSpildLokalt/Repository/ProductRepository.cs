﻿using StopSpildLokalt.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace StopSpildLokalt.Repository
{
    public static class ProductRepository
    {
        private static List<Product> products;

        private static string connectionCredentials = "Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04";

        static ProductRepository()
        {
            products = new List<Product>();

            if (RepositoryOptions.UseDatabase)
            {
                LoadAllProduct();
            }
        }

        public static Product GetByID(int productID)
        {
            Product productObj = products.Find(product => product.ProductID == productID);
            if (productObj != null)
            {
                return productObj;
            }
            else
            {
                return null;
            }
        }

        public static List<Product> GetAll()
        {
            return products;
        }

        public static void LoadAllProduct()
        {
            using (SqlConnection connection = new SqlConnection(connectionCredentials)) using(SqlConnection secondConnection = new SqlConnection(connectionCredentials))
            {
                SqlDataReader reader;
                connection.Open();
                secondConnection.Open();

                string query = "SELECT ProductId, Barcode, Amount, LotNumber, DonationId, BestBeforeDate, ExpirationDate, DiscardedAmount FROM PRODUCT";

                using (SqlCommand sqlCommand = new SqlCommand(query, connection))
                {
                    reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Product productObj = new Product();
                        productObj.ProductID = reader.GetInt32(0);
                        productObj.Barcode = reader.GetString(1); //FK for PRODUCT from CATALOGITEM(Barcode)
                        productObj.Amount = reader.GetInt32(2);
                        productObj.LotNumber = reader.GetString(3); 
                        productObj.DonationID = reader.GetInt32(4);

                        if(reader[5] == DBNull.Value)
                        {
                            productObj.BestBeforeDate = null;
                        }
                        else
                        {
                            productObj.BestBeforeDate = reader.GetDateTime(5);
                        }
                        if (reader[6] == DBNull.Value)
                        {
                            productObj.ExpirationDate = null;
                        }
                        else
                        {
                            productObj.ExpirationDate = reader.GetDateTime(6);
                        }

                        productObj.DiscardedAmount = reader.GetInt32(7);

                        products.Add(productObj);
                    }
                    reader.Close();
                }
            }
        }

        //Precondition: given donation argument must exist in donations list
        public static bool Delete(Product product)
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();
                string statement = $"DELETE FROM PRODUCT WHERE ProductId={product.ProductID}";

                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
            }
            if (rowsAffected > 0)
            {
                products.Remove(product);
            }
            return rowsAffected > 0;
        }

        public static int Add(Product product)
        {
            int productID = -1;
            int rowsAffected;

            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                SqlDataReader reader;

                connection.Open();

                string insertProduct = "INSERT INTO PRODUCT(Barcode, Amount, LotNumber, DonationId, BestBeforeDate, ExpirationDate, DiscardedAmount) " +
                    "VALUES(@Barcode, @Amount, @LotNumber, @DonationId, @BestBeforeDate, @ExpirationDate, @DiscardedAmount);";

                using (SqlCommand sqlCommand = new SqlCommand(insertProduct, connection))
                {

                    //Temporary, move this to VM layer as sanitation
                    if (string.IsNullOrWhiteSpace(product.LotNumber))
                    {
                        product.LotNumber = "N/A";
                    }
                    //^^

                //Inserts an entry into the PRODUCT table
                    sqlCommand.Parameters.AddWithValue("@Barcode", product.Barcode);
                    sqlCommand.Parameters.AddWithValue("@Amount", product.Amount);
                    sqlCommand.Parameters.AddWithValue("@LotNumber", product.LotNumber);
                    sqlCommand.Parameters.AddWithValue("@DonationId", product.DonationID);

                    if (product.BestBeforeDate == null)
                    {
                        sqlCommand.Parameters.Add("@BestBeforeDate", SqlDbType.DateTime2).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@BestBeforeDate", product.BestBeforeDate);
                    }

                    if(product.ExpirationDate == null)
                    {
                        sqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime2).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@ExpirationDate", product.ExpirationDate);
                    }

                  
                    
                    sqlCommand.Parameters.AddWithValue("@DiscardedAmount", product.DiscardedAmount);
                    
                    rowsAffected = sqlCommand.ExecuteNonQuery();

                    //Gets the auto increment identity value ProductId from the newly inserted entry in the PRODUCT table. 
                    if (rowsAffected > 0)
                    {
                        sqlCommand.CommandText = "SELECT CAST(@@IDENTITY AS INT);";
                        reader = sqlCommand.ExecuteReader();
                        reader.Read();
                        productID = reader.GetInt32(0); //First column represents LotId in the LOT table.
                        reader.Close();
                        product.ProductID = productID;
                        products.Add(product);
                    }
                }
            }
            return productID;
        }

        public static bool Update(Product oldProduct, Product newProduct)
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();

                string updateProduct = $"UPDATE PRODUCT SET Barcode = @Barcode, Amount = @Amount, LotNumber = @LotNumber, " +
                    $"BestBeforeDate = @BestBeforeDate, ExpirationDate = @ExpirationDate, DonationId = @DonationId, DiscardedAmount = @DiscardedAmount WHERE ProductId={oldProduct.ProductID}";

                using (SqlCommand sqlCommand = new SqlCommand(updateProduct, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@Barcode", newProduct.Barcode);
                    sqlCommand.Parameters.AddWithValue("@Amount", newProduct.Amount);
                    sqlCommand.Parameters.AddWithValue("@LotNumber", newProduct.LotNumber);
                    sqlCommand.Parameters.AddWithValue("@DonationId", newProduct.DonationID);

                    if (newProduct.BestBeforeDate == null)
                    {
                        sqlCommand.Parameters.Add("@BestBeforeDate", SqlDbType.DateTime2).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@BestBeforeDate", newProduct.BestBeforeDate);
                    }

                    if (newProduct.ExpirationDate == null)
                    {
                        sqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime2).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@ExpirationDate", newProduct.ExpirationDate);
                    }

                    sqlCommand.Parameters.AddWithValue("@DiscardedAmount", newProduct.DiscardedAmount);
                    
                    rowsAffected = sqlCommand.ExecuteNonQuery();

                }
            }

            if (rowsAffected > 0)
            {
                for (int i = 0; i < products.Count; i++)
                {
                    if (products[i].ProductID == oldProduct.ProductID)
                    {
                        products[i] = newProduct;
                    }
                }
            }
            return rowsAffected > 0;
        }

    }
}
