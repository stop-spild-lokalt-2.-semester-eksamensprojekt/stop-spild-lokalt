﻿using StopSpildLokalt.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace StopSpildLokalt.Repository
{
    public static class CatalogItemRepository
    {
        private static List<CatalogItem> catalogItems;
        private static string connectionCredentials = "Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04";

        static CatalogItemRepository()
        {
            catalogItems = new List<CatalogItem>();
            LoadAllCatalogItem();
        }
        public static List<CatalogItem> GetAll()
        {
            return catalogItems;
        }
        public static CatalogItem GetById(string barcode)
        {
            CatalogItem found;
            found = catalogItems.Find(item => item.Barcode == barcode);
            if (found != null)
            {
                return found;
            }
            else
            {
                return null;
            }
        }

        public static void LoadAllCatalogItem()
        {
            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();
                string query = "SELECT Barcode, CatalogItemName, OriginCountry, Weight FROM CATALOGITEM";

                using(SqlCommand sqlCommand = new SqlCommand(query, connection))
                {
                    SqlDataReader reader;
                    reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        CatalogItem catalogItemObj = new CatalogItem(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
                        catalogItems.Add(catalogItemObj);
                    }
                    reader.Close();
                }
            }
        }

        /*
         * Inserts a new entry containing the data within the given catalogItem instance to the database.
        */
        public static bool Add(CatalogItem catalogItem)
        {
            int rowsAffected;
            CatalogItem alreadyExists = CatalogItemRepository.GetById(catalogItem.Barcode);
            string statement = "INSERT INTO CATALOGITEM(Barcode, CatalogItemName, OriginCountry, Weight) VALUES(@barcode, @catalogItemName, @originCountry, @weight);";

            if (alreadyExists == null)
            {
                using (SqlConnection connection = new SqlConnection(connectionCredentials))
                {
                    connection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                    {

                        if (catalogItem.OriginCountry == null)
                        {
                            catalogItem.OriginCountry = "N/A";
                        }

                        sqlCommand.Parameters.AddWithValue("@barcode", catalogItem.Barcode);
                        sqlCommand.Parameters.AddWithValue("@catalogItemName", catalogItem.Name);
                        sqlCommand.Parameters.AddWithValue("@originCountry", catalogItem.OriginCountry);
                        sqlCommand.Parameters.AddWithValue("@weight", catalogItem.Weight);
                        rowsAffected = sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            else 
            {
                catalogItem = alreadyExists; //Needed in case obj given to Add method needs a barcode PK from existing reference.
                return false;
            }

            if(rowsAffected > 0)
            {
                catalogItems.Add(catalogItem);
            }

            return rowsAffected > 0;
        }

        public static bool Remove(CatalogItem catalogItem)
        {
            int rowsAffected;

            string statement = $"DELETE FROM CATALOGITEM WHERE Barcode = @barcode";
            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();
                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@barcode", catalogItem.Barcode);
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
            }
            if(rowsAffected > 0)
            {
                catalogItems.Remove(catalogItem);
            }
            return rowsAffected > 0;
        }

        public static void Update(CatalogItem oldItem, CatalogItem newItem)
        {
            string statement = $"UPDATE CATALOGITEM SET CatalogItemName = @catalogItemName, OriginCountry = @originCountry, Weight = @weight " +
            $"WHERE Barcode = @barcode";
            int rowsAffected;

            using (SqlConnection sqlConnection = new SqlConnection(connectionCredentials))
            {
                sqlConnection.Open();
                using (SqlCommand sqlCommand = new SqlCommand(statement, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@barcode", newItem.Barcode);
                    sqlCommand.Parameters.AddWithValue("@catalogItemName", newItem.Name);
                    sqlCommand.Parameters.AddWithValue("@originCountry", newItem.OriginCountry);
                    sqlCommand.Parameters.AddWithValue("@weight", newItem.Weight);
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
            }

            if(rowsAffected > 0)
            {
                for (int i = 0; i < catalogItems.Count; i++)
                {
                    if (catalogItems[i].Barcode == oldItem.Barcode)
                    {
                        catalogItems[i] = newItem;
                    }
                }
            }
        }


    }
}
