﻿using StopSpildLokalt.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace StopSpildLokalt.Repository
{
    public static class DonationRepository
    {
        private static List<Donation> donations;
        private static string connectionCredentials = "Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04";

        static DonationRepository()
        {
            donations = new List<Donation>();
            if (RepositoryOptions.UseDatabase)
            {
                LoadAllDonation();
            }
            
        }

        public static Donation GetByID(int id)
        {
            Donation donationObj = donations.Find(donation => donation.DonationID == id);
            if(donationObj != null)
            {
                return donationObj;
            }
            else
            {
                return null;
            }
        }

        public static List<Donation> GetAll()
        {
            return donations;
        }

        public static void LoadAllDonation()
        {
            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                SqlDataReader reader;

                connection.Open();
                string statement = "SELECT DonationId, DonationDate, EmployeeInitials, DonorId FROM DONATION";

                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Donation donationObj = new Donation(reader.GetInt32(0), reader.GetDateTime(1), reader.GetString(2), reader.GetInt32(3));
                        donations.Add(donationObj);
                    }
                }
            }
        }

        //Precondition: given donation argument must exist in donations list
        public static bool Delete(Donation donation)
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();
                string statement = $"DELETE FROM DONATION WHERE DonationId={donation.DonationID}";

                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
            }
            if (rowsAffected > 0)
            {
                donations.Remove(donation);
            }
            return rowsAffected > 0;
        }

        /*
         * Adds an instance of donation to the internal list donations & updates the DONATION table in the database with an entry. 
         * Returns the DonationId of the added donation if it suceeds, or -1 in case of failiure.
        */
        public static int Add(Donation donation)
        {
            int donationId = -1;
            int rowsAffected;

            using(SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                SqlDataReader reader;

                connection.Open();
                string statement = "INSERT INTO DONATION(DonationDate, EmployeeInitials, DonorId) VALUES(@date, @initials, @donorId)";

                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@date", donation.DonationDate); 
                    sqlCommand.Parameters.AddWithValue("@initials", donation.DonationInitials);
                    sqlCommand.Parameters.AddWithValue("@donorId", donation.DonorID);
                    rowsAffected = sqlCommand.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        statement = "SELECT CAST(@@IDENTITY AS INT);";
                        using (SqlCommand secondCommand = new SqlCommand(statement, connection))
                        {
                            reader = secondCommand.ExecuteReader();
                            reader.Read();
                            donationId = reader.GetInt32(0); 
                            donation.DonationID = donationId;
                            donations.Add(donation);
                        }
                    }
                }
            }
            return donationId;
        }

        //Precondition: the argument given for oldDonation must exist in the list donations.
        public static void Update(Donation oldDonation, Donation newDonation)
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection(connectionCredentials))
            {
                connection.Open();
                string statement = $"UPDATE DONATION SET DonationDate = @DonationDate, EmployeeInitials = @EmployeeInitials, DonorId = @DonorId  WHERE DonationId={oldDonation.DonationID}";

                using (SqlCommand sqlCommand = new SqlCommand(statement, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@DonationDate", newDonation.DonationDate); //Check to make sure whether or not the format fits the SQL format
                    sqlCommand.Parameters.AddWithValue("@EmployeeInitials", newDonation.DonationInitials);
                    sqlCommand.Parameters.AddWithValue("@DonorId", newDonation.DonorID);
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
            }

            if (rowsAffected > 0) 
            {
                for (int i = 0; i < donations.Count; i++)
                {
                    if (donations[i].DonationID == oldDonation.DonationID)
                    {
                        donations[i] = newDonation;
                    }
                }
            }
        }

    }
}
