﻿using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.Model
{
    public class CatalogItem
    {
        public string Barcode { get; set; } //PK
        public string Name { get; set; }
        public string OriginCountry { get; set; }

        public int Weight { get; set; }

        public CatalogItem(string barcode, string name, string originCountry, int weight)
        {
            Name = name;
            Barcode = barcode;
            OriginCountry = originCountry;
            Weight = weight;
        }

        public CatalogItem(string barcode, string name, string originCountry, int weight, bool addToRepo = false)
        {
            Name = name;
            Barcode = barcode;
            OriginCountry = originCountry;
            Weight = weight;

            if (addToRepo)
            {
                CatalogItemRepository.Add(this);  
            }

        }
    }
}
