﻿using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.Model
{
    public class Donor
    {
        public Donor(int donorID, string donorName, string donorAddress, string phoneNumber, bool addToRepo = false)
        {
            this.DonorID = donorID;
            this.DonorName = donorName;
            this.DonorAddress = donorAddress;
            this.DonorPhone = phoneNumber;

            if(addToRepo)
            {
                DonorRepository.Add(this);
            }
        }

        public Donor(int donorID, string donorName, string donorAddress, string phoneNumber)
        {
            this.DonorID = donorID;
            this.DonorName = donorName;
            this.DonorAddress = donorAddress;
            this.DonorPhone = phoneNumber;
        }
        public Donor(string donorName, string donorAddress, string phoneNumber)
        {
            this.DonorName = donorName;
            this.DonorAddress = donorAddress;
            this.DonorPhone = phoneNumber;
        }
        public Donor()
        {

        }
        public int DonorID { get; set; }

        public string DonorName { get; set; }

        public string DonorAddress { get; set; }

        public string DonorPhone { get; set; }

    }
}
