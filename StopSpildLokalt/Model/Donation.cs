﻿using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.Model
{
    public class Donation
    {
        //Constructors
        #region Constructors
        public Donation(int donationID, DateTime donationDate, string donationInitials, int donorID)
        {
            this.donationID = donationID;
            this.donationDate = donationDate;
            this.donationInitials = donationInitials;
            this.donorID = donorID;
        }

        public Donation(DateTime donationDate, string donationInitials, int donorID, bool addToRepo = false)
        {
            this.donationDate = donationDate;
            this.donationInitials = donationInitials;
            this.donorID = donorID;
            if (addToRepo)
            {
                this.donationID = DonationRepository.Add(this);
            }

        }

        public Donation()
        {
            //
        }

        #endregion

        //Fields
        #region Fields
        private int donationID;

        public int DonationID
        {
            get { return donationID; }
            set { donationID = value; }
        }

        private DateTime donationDate;

        public DateTime DonationDate
        {
            get { return donationDate; }
            set { donationDate = value; }
        }

        private string donationInitials;

        public string DonationInitials
        {
            get { return donationInitials; }
            set { donationInitials = value; }
        }

        private int donorID;    //Perhaps this field should be replaced by an instance of Donor? We'll see when we get there :-)

        public int DonorID
        {
            get { return donorID; }
            set { donorID = value; }
        }
        #endregion
    }
}
