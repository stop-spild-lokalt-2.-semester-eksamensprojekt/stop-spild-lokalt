﻿using StopSpildLokalt.Repository;
using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace StopSpildLokalt.Model
{
    public class Product
    {
        
        public string Barcode { get; set; } //FK
        public int Amount { get; set; }
        public string LotNumber { get; set; }
        public DateTime? BestBeforeDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int DonationID { get; set; } //FK
        public int ProductID { get; set; } //PK
        public int DiscardedAmount { get; set; }


        public Product(string barcode, int amount, string lotNumber, DateTime? bestBeforeDate, DateTime? expirationDate, int donationID, int productID)
        {
            Barcode = barcode;
            Amount = amount;
            LotNumber = lotNumber;
            BestBeforeDate = bestBeforeDate;
            ExpirationDate = expirationDate;
            DonationID = donationID;
            ProductID = productID;
        }

        public Product(string barcode, int amount, string lotNumber, DateTime? bestBeforeDate, DateTime? expirationDate, int donationID, int productID, int discardedAmount)
        {
            Barcode = barcode;
            Amount = amount;
            LotNumber = lotNumber;
            BestBeforeDate = bestBeforeDate;
            ExpirationDate = expirationDate;
            DonationID = donationID;
            ProductID = productID;
            DiscardedAmount = discardedAmount;
        }

        public Product(string barcode, int amount, string lotNumber, DateType datetype, DateTime? date, int donationID, bool addToRepo = false)
        {
            Barcode = barcode;
            Amount = amount;
            LotNumber = lotNumber;
            switch (datetype)
            {
                case DateType.Ingen:
                    ExpirationDate = null;
                    BestBeforeDate = null;
                    break;
                case DateType.BF:
                    BestBeforeDate = date;
                    ExpirationDate = null;
                    break;
                case DateType.SA:
                    BestBeforeDate = null;
                    ExpirationDate = date;
                    break;
            }
            DonationID = donationID;
            if (addToRepo)
            {
                ProductID = ProductRepository.Add(this);
            }
        }

        public Product(string barcode, int amount, string lotNumber, DateType datetype, DateTime? date, int donationID, int discardedAmount, bool addToRepo = false)
        {
            Barcode = barcode;
            Amount = amount;
            LotNumber = lotNumber;
            switch (datetype)
            {
                case DateType.Ingen:
                    ExpirationDate = null;
                    BestBeforeDate = null;
                    break;
                case DateType.BF:
                    BestBeforeDate = date;
                    ExpirationDate = null;
                    break;
                case DateType.SA:
                    BestBeforeDate = null;
                    ExpirationDate = date;
                    break;
            }
            DonationID = donationID;
            DiscardedAmount = discardedAmount;

            if (addToRepo)
            {
                ProductID = ProductRepository.Add(this);
            }
        }

        public Product(string barcode, int amount, string lotNumber, DateTime? date, DateType datetype, int donationID, int productID, int discardedAmount)
        {
            Barcode = barcode;
            Amount = amount;
            LotNumber = lotNumber;

            switch (datetype)
            {
                case DateType.Ingen:
                    ExpirationDate = null;
                    BestBeforeDate = null;
                    break;
                case DateType.BF:
                    BestBeforeDate = date;
                    ExpirationDate = null;
                    break;
                case DateType.SA:
                    BestBeforeDate = null;
                    ExpirationDate = date;
                    break;
            }

            DonationID = donationID;
            ProductID = productID;
            DiscardedAmount = discardedAmount;
        }

        public Product()
        {

        }
    }
}
