using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;

namespace UnitTest_DonorRepository
{
    [TestClass]
    public class DonorRepositoryTests
    {
        [TestMethod]
        public void Add_CanBeAddedWithWithNullValues_ReturnFalse()
        {
            //Arrange
            //Testing static class
            Donor testDonor = new Donor("Does", null, "work");
            int result;

            //Act
            result = DonorRepository.Add(testDonor);

            //Assert
            Assert.IsFalse(result > 0);
        }

        [TestMethod]
        public void Remove_RemovingNonExistingDonor_ReturnFalse()
        {
            //Arrange
            //Testing static class
            Donor testDonor = new Donor(1337,"Does", null, "work"); //Assuming DonorID 1337 doesnt exist in DB.
            bool result;

            //Act
            result = DonorRepository.Remove(testDonor);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Remove_RemovingSameDonorTwice_ReturnFalse()
        {
            //Arrange
            //Testing static class
            Donor testDonor = new Donor("Does", "it", "work");
            bool result;

            //Act
            DonorRepository.Add(testDonor);
            result = DonorRepository.Remove(testDonor);
            result = DonorRepository.Remove(testDonor);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void UpdateDonor_UpdatingExistingUser_AreEqual()
        {
            //Arrange
            //Testing static class
            Donor testDonor1 = new Donor("Does", "it", "work");
            Donor testDonor2 = new Donor("Does", "it", "yes");
            Donor testDonor3 = new Donor();

            //Act
            DonorRepository.Add(testDonor1);
            DonorRepository.UpdateDonor(testDonor1, testDonor2);
            testDonor3 = DonorRepository.GetByID(testDonor1.DonorID);

            //Assert
            Assert.AreEqual("yes", testDonor3.DonorPhone);
        }

        [TestMethod]
        public void UpdateDonor_UpdateNonExistingDonor_ReturnFalse()
        {
            //Arrange
            //Testing static class
            Donor testDonor1 = new Donor("Er denne", "navngivning bedre", "Kasper?");
            Donor testDonor2 = new Donor();//Non existing donor
            bool result;

            //Act
            result = DonorRepository.UpdateDonor(testDonor2, testDonor1);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Add_TestParameters_ReturnTrue()
        {
            //Arrange
            //Testing static class
            Donor dropTable = new Donor("Ima do it", "); DROP TABLE DONOR;--", "i done did it");
            List<Donor> tempList;

            //Act
            DonorRepository.Add(dropTable);
            DonorRepository.LoadAllDonor();
            tempList = DonorRepository.GetAll();

            //Assert
            Assert.IsTrue(tempList.Count > 1);

        }
    }
}
