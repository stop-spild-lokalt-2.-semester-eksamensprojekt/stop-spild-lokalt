using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using StopSpildLokalt.ViewModel;
using System.Data.SqlClient;

namespace UnitTests
{

    [TestClass]
    public class ProoductRepository_UnitTest
    {
        [TestMethod]
        public void CanAddToRepository()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_ME", 1);
            DonationRepository.Add(donationObj);

            CatalogItem catalogItemObj = new CatalogItem("T01373492", "Frozen berries", "Spain", 600);
            CatalogItemRepository.Add(catalogItemObj);

            Product productObj = new Product(catalogItemObj.Barcode, 5, "L51232", DateType.BF,
                new DateTime(2021, 7, 20), donationObj.DonationID);
            
            Product productInRepo;

            //Act
            ProductRepository.Add(productObj);

            //Assert
            productInRepo = ProductRepository.GetAll().Find(item => item.ProductID == productObj.ProductID);
            Assert.AreEqual(productInRepo, productObj);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanGetById()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_ME2", 1);
            DonationRepository.Add(donationObj);

            CatalogItem catalogItemObj = new CatalogItem("T099321", "Frozen bananas", "Spain", 500);
            CatalogItemRepository.Add(catalogItemObj);

            Product productObj = new Product(catalogItemObj.Barcode, 5, "L2231z", DateType.BF,
                new DateTime(2021, 7, 15), donationObj.DonationID);

            ProductRepository.Add(productObj);

            Product gottenById;

            //Act
            gottenById = ProductRepository.GetByID(productObj.ProductID);

            //Assert
            Assert.AreEqual(productObj, gottenById);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanDeleteFromRepository()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_ME3", 1);
            DonationRepository.Add(donationObj);

            CatalogItem catalogItemObj = new CatalogItem("T0442321", "Frozen apples", "Spain", 500);
            CatalogItemRepository.Add(catalogItemObj);

            Product productObj = new Product(catalogItemObj.Barcode, 5, "L2423z", DateType.BF,
                new DateTime(2021, 8, 15), donationObj.DonationID);

            ProductRepository.Add(productObj);

            //Act
            ProductRepository.Delete(productObj);

            //Assert
            Assert.IsNull(ProductRepository.GetAll().Find(item => item.ProductID == productObj.ProductID));

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanUpdate()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_ME4", 1);
            DonationRepository.Add(donationObj);

            CatalogItem catalogItemObj = new CatalogItem("T01373492", "Frozen mushrooms", "Spain", 500);
            CatalogItemRepository.Add(catalogItemObj);

            Product productObj = new Product(catalogItemObj.Barcode, 5, "L2241", DateType.BF,
                new DateTime?(new DateTime(2023, 7, 20)), donationObj.DonationID);
            ProductRepository.Add(productObj);

            Product updatedProductObj = new Product(catalogItemObj.Barcode, 10, "LXXXXX2441", 
                DateType.SA, new DateTime?(new DateTime(2023, 7, 20)), donationObj.DonationID, productObj.ProductID);

            //Act
            ProductRepository.Update(productObj, updatedProductObj);

            //Assert
            Assert.AreEqual(ProductRepository.GetAll().Find(item => item.ProductID == productObj.ProductID), updatedProductObj);

            //Reset
            ResetTests();

        }

        public void ResetTests()
        {
            int rowsAffectedProduct;
            int rowsAffectedCatalog;
            int rowsAffectedDonation;
             
            using (SqlConnection connection = new SqlConnection("Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04"))
            {
                connection.Open();
                string statement = "DELETE FROM PRODUCT WHERE Barcode LIKE 'T%';";
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    rowsAffectedProduct = command.ExecuteNonQuery();
                    statement = "DELETE FROM CATALOGITEM WHERE Barcode LIKE 'T%';";
                    command.CommandText = statement;
                    rowsAffectedCatalog = command.ExecuteNonQuery();
                    statement = "DELETE FROM DONATION WHERE EmployeeInitials LIKE 'Test%'";
                    command.CommandText = statement;
                    rowsAffectedDonation = command.ExecuteNonQuery();
                }
            }
            //Assert.IsTrue(rowsAffectedCatalog > 0 && rowsAffected );
        }
    }
}
