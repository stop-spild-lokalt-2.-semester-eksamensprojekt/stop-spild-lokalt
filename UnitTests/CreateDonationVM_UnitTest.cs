﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using StopSpildLokalt.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace UnitTests
{

    [TestClass]
    public class CreateDonationVM_UnitTest
    {
        [TestMethod]
        public void CanCreateDonationAndAddToProductRepository()
        {
            //Arrange
            Donor donorObj = new Donor("Test_Bilka Odense", "Bilkavej 111", "12345678");
            DonorRepository.Add(donorObj);
            DonorVM donorVMObj = new DonorVM("Test_Bilka Odense", "Bilkavej 111", "12345678", donorObj.DonorID);

            ProductVM productVM = new ProductVM("Skim milk", "T01113334", "Denmark", 4, 1000,
                "L500e1", new DateTime(2021, 7, 20), DateType.BF, 0);

            List<ProductVM> productVMCollection = new List<ProductVM>();
            productVMCollection.Add(productVM);

            CreateDonationVM createDonationVM = new CreateDonationVM();

            Donation donationObj;

            bool foundProduct = false;

            //Act
            donationObj = createDonationVM.CreateDonationAndAddToRepo(productVMCollection, donorVMObj, DateTime.Now, "Test_MA");

            //Assert

            //Checks to see if a Donation instance matching the one created above exists in DonationRepository. 
            Assert.AreEqual(DonationRepository.GetAll().Find(item => item.DonationID == donationObj.DonationID), donationObj);

            //Checks to see if Product instances matching the ProductVMs given to the CreateDonationAndAddToRepo exist in ProductRepository.
            foreach (var item in ProductRepository.GetAll())
            {
                if (item.DonationID == donationObj.DonationID && item.Amount == productVM.Amount
                && item.LotNumber == productVM.LotNumber && item.BestBeforeDate == productVM.Date)
                {
                    foundProduct = true;
                }
            }
            Assert.IsTrue(foundProduct);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanCreateDonationAndAddToCatalogItemRepository()
        {
            //Arrange
            Donor donorObj = new Donor("Test_Bilka Odense", "Bilkavej 111", "12345678");
            DonorRepository.Add(donorObj);
            DonorVM donorVMObj = new DonorVM("Test_Bilka Odense", "Bilkavej 111", "12345678", donorObj.DonorID);

            ProductVM productVM = new ProductVM("Skim milk", "T01113334", "Denmark", 4, 1000,
                "L500e1", new DateTime(2021, 7, 20), DateType.BF, 0);

            List<ProductVM> productVMCollection = new List<ProductVM>();
            productVMCollection.Add(productVM);

            CreateDonationVM createDonationVM = new CreateDonationVM();

            Donation donationObj;

            bool foundCatalogItem = false;

            //Act
            donationObj = createDonationVM.CreateDonationAndAddToRepo(productVMCollection, donorVMObj, DateTime.Now, "Test_MA");

            //Assert

            //Checks to see if CatalogItem instances matching the ProductVMs given to the CreateDonationAndAddToRepo method exist in
            //the CatalogItemRepository
            foreach (var item in CatalogItemRepository.GetAll())
            {
                if (item.Barcode == productVM.Barcode && item.Name == productVM.ProductName
                    && item.OriginCountry == productVM.OriginCountry && item.Weight == productVM.Weight)
                {
                    foundCatalogItem = true;
                }
            }
            Assert.IsTrue(foundCatalogItem);

            //Reset
            ResetTests();
        }

        public void ResetTests()
        {
            using (SqlConnection connection = new SqlConnection("Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04"))
            {
                connection.Open();
                string statement = "DELETE FROM PRODUCT WHERE Barcode LIKE 'T%'";
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.ExecuteNonQuery();
                    statement = "DELETE FROM CATALOGITEM WHERE Barcode LIKE 'T%'";
                    command.CommandText = statement;
                    command.ExecuteNonQuery();
                    statement = "DELETE FROM DONATION WHERE EmployeeInitials LIKE 'Test%'";
                    command.CommandText = statement;
                    command.ExecuteNonQuery();
                    statement = "DELETE FROM DONOR WHERE DonorName LIKE 'Test%'";
                    command.CommandText = statement;
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
