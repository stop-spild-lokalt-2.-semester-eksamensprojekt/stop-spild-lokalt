﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace UnitTests
{

    [TestClass]
    public class DonationRepository_UnitTest
    {
        [TestMethod]
        public void CanAddToRepository()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_John", 1);

            //Act
            DonationRepository.Add(donationObj);


            //Assert
            Assert.AreEqual(DonationRepository.GetAll().Find(item => item.DonationID == donationObj.DonationID), donationObj);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanGetById()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_John1", 1);
            Donation gottenById;
            DonationRepository.Add(donationObj);

            //Act
            gottenById = DonationRepository.GetByID(donationObj.DonationID);

            //Assert
            Assert.AreEqual(donationObj, gottenById);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanDeleteFromRepository()
        {
            //Arrange
            Donation donationObj = new Donation(DateTime.Now, "Test_John1", 1);
            DonationRepository.Add(donationObj);

            //Act
            DonationRepository.Delete(donationObj);

            //Assert
            Assert.IsNull(DonationRepository.GetAll().Find(item => item.DonationID == donationObj.DonationID));
        }

        [TestMethod]
        public void CanUpdate()
        {
            //Arrange
            Donation donationObj = new Donation(new DateTime(2021, 1, 1), "Test_John", 1);
            DonationRepository.Add(donationObj);

            Donation updatedDonationObj = new Donation(donationObj.DonationID, DateTime.Now, "Test_Johnny", 2);

            //Act
            DonationRepository.Update(donationObj, updatedDonationObj);

            //Assert
            Assert.AreEqual(DonationRepository.GetAll().Find(item => item.DonationID == donationObj.DonationID), updatedDonationObj);

            //Reset
            ResetTests();

        }

        public void ResetTests()
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection("Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04"))
            {
                connection.Open();
                string statement = "DELETE FROM DONATION WHERE EmployeeInitials LIKE 'Test%';";
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
            }
            //Assert.IsTrue(rowsAffected > 0);
        }
    }
}
