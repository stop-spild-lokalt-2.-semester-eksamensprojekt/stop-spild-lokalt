﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopSpildLokalt.Model;
using StopSpildLokalt.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace UnitTests
{

    [TestClass]                     
    public class CatalogItemRepository_UnitTest
    {

        [TestMethod]
        public void CanAddToRepository()
        {
            //Arrange
            CatalogItem catalogItemObj = new CatalogItem("T0110500", "Cookies", "Denmark", 500);

            //Act
            CatalogItemRepository.Add(catalogItemObj);

            //Assert
            Assert.AreEqual(CatalogItemRepository.GetAll().Find(item => item.Barcode == catalogItemObj.Barcode), catalogItemObj);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CantAddDuplicates()
        {
            //Arrange
            CatalogItem catalogItemObj = new CatalogItem("T1235722", "Pancakes", "Denmark", 500);
            int occurances = 0;

            //Act
            CatalogItemRepository.Add(catalogItemObj);
            CatalogItemRepository.Add(catalogItemObj);
            CatalogItemRepository.Add(catalogItemObj);

            foreach (var item in CatalogItemRepository.GetAll())
            {
                if (item == catalogItemObj)
                {
                    occurances++;
                }
            }

            //Assert
            Assert.IsTrue(occurances <= 1 && occurances > 0);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanGetById()
        {
            //Arrange
            CatalogItem catalogItemObj = new CatalogItem("T13372223", "Milkyway", "Denmark", 500);
            CatalogItemRepository.Add(catalogItemObj);
            CatalogItem gottenById;

            //Act
            gottenById = CatalogItemRepository.GetById(catalogItemObj.Barcode);


            //Assert
            Assert.AreEqual(catalogItemObj, gottenById);

            //Reset
            ResetTests();
        }

        [TestMethod]
        public void CanRemoveFromRepository()
        {
            //Arrange
            CatalogItem catalogItemObj = new CatalogItem("T0049423", "Coke zero", "Denmark", 500);
            CatalogItemRepository.Add(catalogItemObj);
            CatalogItem found;

            //Act
            CatalogItemRepository.Remove(catalogItemObj);

            //Assert
            found = CatalogItemRepository.GetAll().Find(item => item.Barcode == catalogItemObj.Barcode);
            Assert.IsNull(found);   
        }

        [TestMethod]
        public void CanUpdate()
        {
            //Arrange
            CatalogItem catalogItemObj = new CatalogItem("T019357732", "Neutral Sho Gel", "Norway", 400);
            CatalogItem updatedCatalogItemObj = new CatalogItem("T019357732", "Neutral Shower Gel", "Denmark", 500);
            CatalogItemRepository.Add(catalogItemObj);

            //Act
            CatalogItemRepository.Update(catalogItemObj, updatedCatalogItemObj);

            //Assert
            Assert.AreEqual(CatalogItemRepository.GetAll().Find(item => item.Barcode == catalogItemObj.Barcode), updatedCatalogItemObj);

            //Reset
            ResetTests();
        }

        //Deletes test catalogitem entries inserted into the database during tests, such that the tests above can be rerun without
        //db exceptions for insertion of duplicate rows with existing primary key barcode values.
        public void ResetTests() 
        {
            int rowsAffected;
            using (SqlConnection connection = new SqlConnection("Server=10.56.8.35;Database=A_EKSDB04_2021;User Id = A_EKS04; Password=A_OPENDB04"))
            {
                connection.Open();
                string statement = "DELETE FROM CATALOGITEM WHERE Barcode LIKE 'T%';";
                using(SqlCommand command = new SqlCommand(statement, connection))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
            }
            //Assert.IsTrue(rowsAffected>0);
        }
    }
}
